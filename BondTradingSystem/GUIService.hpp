//
// Created by ziyi on 2017/12/15.
//

#ifndef UNTITLED_GUISERVICE_HPP
#define UNTITLED_GUISERVICE_HPP

#include <chrono>    // used to calculate the time
#include "pricingservice.hpp"
#include <iostream>
using std::cout;
using std::endl;
using namespace std::chrono;

/**
 * Service for executing orders on an exchange.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class GUIService : public Service<string,Price<T> >
{
private:
    long long int service_start_time;
    long long int last_quote_time;    // the time that last price quoted
    GUIConnector<T>* gui_connector; // connector to publish data

public:
    // input data, if the time is larger than the  300 millisecond throttle, output the data
    explicit GUIService(GUIConnector<T>* gui_connector_):gui_connector(gui_connector_)
    {
        service_start_time = duration_cast< milliseconds >(system_clock::now().time_since_epoch()).count();
        last_quote_time = service_start_time;
    }
    void ProvideData(Price<T> data)
    {
        steady_clock::time_point now =  steady_clock::now();

        long long int current_epic = duration_cast< milliseconds >(system_clock::now().time_since_epoch()).count();

        if(current_epic-last_quote_time > 300)
        {
            // call connector to publish data
            last_quote_time = current_epic;
            gui_connector->Publish(data);
        }
    }
};

#endif //UNTITLED_GUISERVICE_HPP
