/**
 * tradebookingservice.hpp
 * Defines the data types and Service for trade booking.
 *
 * @author Breman Thuraisingham
 */
#ifndef TRADE_BOOKING_SERVICE_HPP
#define TRADE_BOOKING_SERVICE_HPP

#include <string>
#include <vector>
#include "soa.hpp"

// Trade sides
enum Side { BUY, SELL };

/**
 * Trade object with a price, side, and quantity on a particular book.
 * Type T is the product type.
 */
template<typename T>
class Trade
{

public:

  // ctor for a trade
  Trade(const T &_product, string _tradeId, double _price, string _book, long _quantity, Side _side);

  // Get the product
  const T& GetProduct() const;

  // Get the trade ID
  const string& GetTradeId() const;

    // Get the mid price
    double GetPrice() const;

  // Get the book
  const string& GetBook() const;

  // Get the quantity
  long GetQuantity() const;

  // Get the side
  Side GetSide() const;

private:
  T product;
  string tradeId;
  string book;
  long quantity;
  Side side;
    double price;

};

/**
 * Trade Booking Service to book trades to a particular book.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class TradeBookingService : public Service<string,Trade <T> >
{
private:
    map<string, Trade<T>> trades;
public:
    // Book the trade
    void BookTrade(Trade<T> &trade);

    // Get data on our service given a key
    Trade <T>& GetData(string key);

    // The callback that a Connector should invoke for any new or updated data
    void OnMessage(Trade <T> &data);

};

template<typename T>
Trade<T>::Trade(const T &_product, string _tradeId, double _price, string _book, long _quantity, Side _side) :
  product(_product)
{
  tradeId = _tradeId;
  book = _book;
  quantity = _quantity;
  side = _side;
    price = _price;
}

template<typename T>
const T& Trade<T>::GetProduct() const
{
  return product;
}

template<typename T>
const string& Trade<T>::GetTradeId() const
{
  return tradeId;
}

template<typename T>
const string& Trade<T>::GetBook() const
{
  return book;
}

template<typename T>
double Trade<T>::GetPrice() const
{
    return price;
}

template<typename T>
long Trade<T>::GetQuantity() const
{
  return quantity;
}

template<typename T>
Side Trade<T>::GetSide() const
{
  return side;
}

/*
 *  notify all of the listeners when booking the trade
 */
template<typename T>
void TradeBookingService<T>::BookTrade(Trade<T> &trade)
{
    Service<string,Trade <T>>::Notify(trade);
}

// Get data on our service given a key
template<typename T>
Trade<T>& TradeBookingService<T>::GetData(string key)
{
    if(trades.find(key) != trades.end())
        return trades.find(key)->second;
    else
        cout << "in trade booking service, GetData function, key = " << key << " not found" << endl;
}

// The callback that a Connector should invoke for any new or updated data
template<typename T>
void TradeBookingService<T>::OnMessage(Trade <T> &data)
{
    BookTrade(data);
}

#endif
