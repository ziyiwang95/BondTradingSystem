//
// Created by ziyi on 12/15/17.
//

#ifndef BONDTRADINGSYSTEM_BONDALGOEXECUTIONSERVICE_HPP
#define BONDTRADINGSYSTEM_BONDALGOEXECUTIONSERVICE_HPP

#include <string>
#include <iostream>
#include "soa.hpp"
#include "marketdataservice.hpp"
#include "executionservice.hpp"

using std::cout;
using std::endl;

/*
 * T is the product identifier
 */
template<typename T>
class AlgoExecution
{
private:
    ExecutionOrder<T>* order;
    Market market;
    static int counter;    // count how many orders have been executed
public:
    explicit AlgoExecution(OrderBook<T> data):market(CME)
    {
        counter++;
        T product = data.GetProduct();
        PricingSide side = PricingSide(counter % 2);   // alternating between buy and sell
        string orderId = std::to_string(counter);
        double price;
        long quantity;
        double hidden_ratio = 0.9;    // by default, 90% of the orders are hidden
        OrderType order_type = MARKET;

        // according to the side, get the price and quantity to cross spread and trade
        if(side == BID)
        {
            price = data.GetOfferStack()[0].GetPrice();
            quantity = data.GetOfferStack()[0].GetQuantity();
        }
        else
        {
            price = data.GetBidStack()[0].GetPrice();
            quantity = data.GetBidStack()[0].GetQuantity();
        }


        order = new ExecutionOrder<T>(product, side, orderId, MARKET,price,
                quantity, quantity*hidden_ratio, orderId, false);

    }
    ~AlgoExecution()
    {
        delete order;
    }

    ExecutionOrder<T> GetOrder()
    {
        return *order;
    }
    Market GetMarket()
    {
        return market;
    }

};

template<typename T>
int AlgoExecution<T>::counter = 0;
/*
 * T is the product identifier
 */
template<typename T>
class BondAlgoExecutionService: public Service<string, AlgoExecution<T>>
{
public:

    // check the spread of top of the order book
    // if the spread is ideal, execute it!
    void Execute(OrderBook<T> data)
    {
        // cout << "in algo execution service!" << endl;
        double best_bid = data.GetBidStack()[0].GetPrice();
        double best_offer = data.GetOfferStack()[0].GetPrice();
        double spread = best_offer - best_bid;
        double tol = 1.0 / 127;

        // if the spread is not ideal(i.e. not 1/128), we don't execute a trade
        if(spread > tol)
        {
            return;
        }
        else
        {
            // when the spread is ideal, we create an algo execution object based on
            // the order book
            AlgoExecution<T> algo_execution(data);
            Service<string, AlgoExecution<T>>::Notify(algo_execution);
        }
    }
};


#endif //BONDTRADINGSYSTEM_BONDALGOEXECUTIONSERVICE_HPP
