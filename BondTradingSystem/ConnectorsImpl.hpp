//
// Created by ziyi on 2017/11/29.
//

#ifndef DERIVEDCONNECTORS_HPP
#define DERIVEDCONNECTORS_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <chrono>
#include <tuple>

#include "soa.hpp"
#include "tradebookingservice.hpp"
#include "FormatParser.hpp"
#include "ProductMap.hpp"
#include "pricingservice.hpp"
#include "streamingservice.hpp"
#include "marketdataservice.hpp"
#include "positionservice.hpp"
#include "riskservice.hpp"
#include "executionservice.hpp"
#include "inquiryservice.hpp"

using namespace std::chrono;
using std::cout;
using std::endl;
using std::make_pair;
/*
 * connector between the trades.txt and the tradebookingservice
 * loop through to get all of the trades from the trades.txt and invoke OnMessage function of tradebookingservice
 * for each trade
 * type V is the data type that it sends to book trading service
 */
template<typename V>
class TradeBookingConnector: public Connector<Trade<V>>
{
private:
    string file_name;
    TradeBookingService<V> * tradebookingservice;
public:

    // constructor, initialize the file destination
    explicit TradeBookingConnector(string _file_name, TradeBookingService<V> * _tradebookingservice): file_name(_file_name),
                                                                                                      tradebookingservice(_tradebookingservice)
    {}

    // Publish data to the Connector
    // this is a subscribe only connector and publish is not used
    virtual void Publish(const Trade<V> &data)
    {}

    // traverse trades in the trade book and send trades to the trade booking service
    void TraverseTrades()
    {
        string trade_str_format;    // raw line input from the file
        ifstream f(file_name);
        if (f.is_open()) {
            // store the elements of a line in the file
            vector<string> line_elements;

            while(getline(f, trade_str_format))
            {
                cout << "********************** new trade ***********************" << endl;
                cout << trade_str_format << endl;
                line_elements = FormatParser::Parse(trade_str_format);

                // create a trade object based on the element
                // sample line: 2y, TID_3, TRSY2, 5000000, 99.0, BUY

                // get the product map, using to prepare for the trade
                unordered_map<string, Bond> product_map = ProductMap::GetProductMap();

                // get parameters for the trade
                string productID = line_elements[0];
                Bond product = product_map[productID];
                string tradeID = line_elements[1];
                double price = atof(line_elements[4].c_str());
                string book = line_elements[2];
                long quantity = atol(line_elements[3].c_str());
                Side side;

                // very careful with the comparason!!!!!
                // the line end identifier will make the compare not equal
                if(line_elements[5][0] == 'B')
                {
                    side = BUY;
                }
                else
                {
                    side = SELL;
                }

                // create a trade, flow the trade to trade booking service
                Trade<Bond> trade(product, tradeID, price, book, quantity, side);
                tradebookingservice->OnMessage(trade);
            }
        } else{
            cout << "error!!! the file " << file_name << " can not be opened!" << endl;
        }
    }
};

/*
 * publish data to the specific address provided by the user
 * V is the product type, in this case it is Bond
 */
template<typename V>
class PositionConnector: public Connector<Position<V>>
{
private:
    string file_name;
public:
    explicit PositionConnector(string file_name_):file_name(file_name_)
    {
        ofstream out(file_name, ios::trunc);
    }

    // Publish data to the Connector
    virtual void Publish(const Position<V>& data)
    {

        ofstream out(file_name, ios::app);
        string book_name[3] = {"TRSY1", "TRSY2", "TRSY3"};
        long aggregate = 0;
        aggregate += data.GetPosition(book_name[0]) + data.GetPosition(book_name[1]) + data.GetPosition(book_name[2]);

        // get current epoch time and output relevent data
        milliseconds ms = duration_cast< milliseconds >(system_clock::now().time_since_epoch());

        out << ms.count() << "," << data.GetProduct().GetTicker() << ","
            << data.GetPosition(book_name[0]) << ","
            << data.GetPosition(book_name[1]) << ","
            << data.GetPosition(book_name[2]) << ","
            << data.GetAggregatePosition() << endl;
    }
};


/*
 * publish data to the specific address specified by the user
 * V is the product type
 */
template<typename V>
class RiskConnector: public Connector<PV01<V>>
{
private:
    string file_name;
public:
    explicit RiskConnector(string file_name_):file_name(file_name_)
    {
        ofstream out(file_name, ios::trunc);
    }

    // Publish data to the Connector
    virtual void Publish(const PV01<V>& data)
    {
        ofstream out(file_name, ios::app);
        string product = data.GetProduct().GetTicker();

        // get current epoch time and output relevent data
        milliseconds ms = duration_cast< milliseconds >(system_clock::now().time_since_epoch());

        out << ms.count() << "," << data.GetProduct().GetTicker() << ","
            << data.GetTotalPV01() << endl;
    }
};


/*
 * publish data to positionhistory.txt
 * V is the product type, in this case it is Bond
 */
template<typename V>
class PricingConnector: public Connector<Price <V>>
{
private:
    string file_name;
    PricingService<V>* pricing_service;
public:
    explicit PricingConnector(string file_name_, PricingService<V>* pricing_service_):file_name(file_name_), pricing_service(pricing_service_)
    {}

    // don't need to publish things
    virtual void Publish(const Price <V>& data)
    {}

    void Subscribe()
    {
        string raw_line;    // raw line input from the file
        ifstream f(file_name);
        int total_line = 6000000;
        int curr_line = 0;
        if (f.is_open())
        {
            cout << "********************** prices from prices.txt is coming ***********************" << endl;
            // store the elements of a line in the file
            vector<string> line_elements;

            while(getline(f, raw_line))
            {
                curr_line++;
                if(curr_line%600000 == 0)
                    cout << curr_line/600000 * 10 << "% prices processed" << endl;

//                cout << raw_line << endl;
                line_elements = FormatParser::ParseCommaSepLine(raw_line);

                // get the product map, using to prepare for the trade
                unordered_map<string, Bond> product_map = ProductMap::GetProductMap();

                // get parameters for the trade
                string productID = line_elements[0];
                Bond product = product_map[productID];

                double bid = FormatParser::ParsePriceFormat(line_elements[1]);
                double ask = FormatParser::ParsePriceFormat(line_elements[2]);
                double mid = (bid+ask)/2;
                double spread = ask - bid;

                // create a price object, flow the price object to Pricing service
                //Price(const T &_product, double _mid, double _bidOfferSpread);
                Price<V> price(product, mid, spread);
                pricing_service->OnMessage(price);
            }
        } else{
            cout << "error!!! the file " << file_name << " can not be opened!" << endl;
        }
    }
};


/*
 * publish data to gui.txt
 * V is the product type, in this case it is Bond
 */
template<typename V>
class GUIConnector: public Connector<Price<V>>
{
private:
    string file_name;
public:
    explicit GUIConnector(string file_name_):file_name(file_name_)
    {
        ofstream out(file_name, ios::trunc);
    }

    // Publish data to the Connector
    virtual void Publish(const Price<V>& data)
    {
        ofstream out(file_name, ios::app);

        // get current epoch time and output relevent data
        milliseconds ms = duration_cast< milliseconds >(system_clock::now().time_since_epoch());
        out << ms.count() << "," << data.GetProduct().GetTicker()
                  << "," << data.GetMid() << "," << data.GetBidOfferSpread() << endl;
    }
};


/*
 * output the data from streaming service to streaming.txt
 * This is the publish only connector
 * V is the product identifier
 */
template<typename V>
class StreamingConnector: public Connector<PriceStream<V>>
{
private:
    string file_name;
public:
    explicit StreamingConnector(const string& file_name_):file_name(file_name_)
    {
        ofstream out(file_name, ios::trunc);
    }

    // Publish data to the Connector
    virtual void Publish(const PriceStream<V>& data)
    {
        ofstream out(file_name, ios::app);

        // get current epoch time and output relevent data
        milliseconds ms = duration_cast< milliseconds >(system_clock::now().time_since_epoch());

        // output timestamp, ticker, bid/offer prices
        out << ms.count() << "," << data.GetProduct().GetTicker() << ","
            << data.GetBidOrder().GetPrice() << ","
            << data.GetOfferOrder().GetPrice() << endl;
    }
};


/*
 * T is the product identifier
 * This connector read data from marketdata.txt and send the data to market data service
 */
template<typename T>
class MrktDataConnector: public Connector<OrderBook <T>>
{
private:
    string file_name;
    MarketDataService<T>* marketdataservice;
public:
    explicit MrktDataConnector(const string& file_name_,
                               MarketDataService<T>* marketdataservice_)
            :file_name(file_name_), marketdataservice(marketdataservice_)
    {}

    // Publish data to the Connector
    virtual void Publish(const OrderBook <T>& data)
    {}


    void Subscribe()
    {
        string raw_line;    // raw line input from the file
        ifstream f(file_name);
        int total_line = 6;    // total orderbook number, set with 6 at first
        int curr_line = 0;
        if (f.is_open())
        {
            cout << "********************** order books from marketdata.txt is coming ***********************" << endl;
            // store the elements of a line in the file
            vector<string> line_elements;

            int percentage_finished = 0;
            while(getline(f, raw_line))
            {
                curr_line++;
                if(curr_line == 600000)
                {
                    curr_line = 0;
                    percentage_finished += 10;
                    cout << percentage_finished << "% order books finished processing" << endl;
                }
                string ticker = std::get<0>(FormatParser::ParseOrderBook(raw_line));
                vector<double> prices = std::get<1>(FormatParser::ParseOrderBook(raw_line));

                vector<Order> bid_stack;
                vector<Order> offer_stack;

                for(int i = 0; i < 5; i++)
                {
                    // Order(double _price, long _quantity, PricingSide _side);
                    bid_stack.push_back(Order(prices[i*2], 1000000*(i+1), BID));
                    offer_stack.push_back(Order(prices[i*2+1], 1000000*(i+1), OFFER));
                }

                // get the product map, using to prepare for the trade
                unordered_map<string, Bond> product_map = ProductMap::GetProductMap();
                Bond product = product_map[ticker];

                OrderBook<Bond> order_book(product, bid_stack, offer_stack);

                // create an order book object and pass to algo market data service
                marketdataservice->ProcessOrderBook(order_book);
            }
        } else {
            cout << "error!!! the file " << file_name << " can not be opened!" << endl;
        }
    }
};


/*
 * publish data to executions.txt
 * V is the product type
 */
template<typename V>
class ExecutionConnector: public Connector<ExecutionOrder<V>>
{
private:
    string file_name;
public:
    explicit ExecutionConnector(string file_name_):file_name(file_name_)
    {
        ofstream out(file_name, ios::trunc);
    }

    // Publish data to the Connector
    virtual void Publish(const ExecutionOrder<V>& data)
    {
        ofstream out(file_name, ios::app);
        // get current epoch time and output relevent data
        milliseconds ms = duration_cast< milliseconds >(system_clock::now().time_since_epoch());

        string side;
        if(data.GetPricingSide() == BID)
            side = "BUY";
        else
            side = "SELL";
        out << ms.count() << "," << data.GetProduct().GetTicker()
            << "," << "TID_" << data.GetOrderId() << ",MarketOrder"
            << "," << side << "," << data.GetPrice()
            << "," << data.GetVisibleQuantity() << "," << data.GetHiddenQuantity()
            << endl;
    }
};


/*
 * read data from inquiries.txt and pass the data to BondInquiryService
 * V is the product type
 */
template<typename V>
class InquiryConnector: public Connector<Inquiry<V>>
{
private:
    string file_name;
    InquiryService<V>* inquiry_service;
public:
    explicit InquiryConnector(string file_name_, InquiryService<V>* inquiry_service_)
            :file_name(file_name_), inquiry_service(inquiry_service_)
    {}

    // Publish data to the Connector
    virtual void Publish(const Inquiry<V>& data)
    {
        InquiryState state = data.GetState();
        if(state == RECEIVED)
        {
            // after receiving quotes from inquiry service, we update
            // the state and send inquiries with new states to inquiry service
            Inquiry<V> new_state_inquiry = data;
            new_state_inquiry.SetState(QUOTED);
            inquiry_service->OnMessage(new_state_inquiry);

            new_state_inquiry.SetState(DONE);
            inquiry_service->OnMessage(new_state_inquiry);
        }
    }

    // send the data to the service
    virtual void Subscribe()
    {
        string raw_line;    // raw line input from the file
        ifstream f(file_name);
        if (f.is_open())
        {
            cout << "********************** reading from inquiries.txt is coming ***********************" << endl;
            // store the elements of a line in the file
            vector<string> line_elements;
            string ticker;
            string quote_id;
            string side;
            Side side_enum;
            while(getline(f, raw_line))
            {
                cout << raw_line << endl;
                line_elements = FormatParser::ParseCommaSepLine(raw_line);

                quote_id = line_elements[0];
                ticker = line_elements[1];
                side = line_elements[2];

                if(side[0] == 'B')
                    side_enum = BUY;
                else
                    side_enum = SELL;

                // get the product map, using to prepare for the trade
                unordered_map<string, Bond> product_map = ProductMap::GetProductMap();
                Bond product = product_map[ticker];
                // create an order book object and pass to algo market data service
                Inquiry<Bond> inquiry(quote_id, product, side_enum, 1000000, -1, RECEIVED);

                inquiry_service->OnMessage(inquiry);

                // after receiving quotes from inquiry service, we update
                // the state and send inquiries with new states to inquiry service
                Inquiry<V> new_state_inquiry = inquiry;
                new_state_inquiry.SetState(QUOTED);
                inquiry_service->OnMessage(new_state_inquiry);

                new_state_inquiry.SetState(DONE);
                inquiry_service->OnMessage(new_state_inquiry);

            }
        }
        else
        {
            cout << "error!!! the file " << file_name << " can not be opened!" << endl;
        }
    }
};


/*
 * publish data to allinquiries.txt
 * V is the product type
 */
template<typename V>
class AllInquiriesConnector: public Connector<Inquiry<V>>
{
private:
    string file_name;
public:
    explicit AllInquiriesConnector(string file_name_):file_name(file_name_)
    {
        ofstream out(file_name, ios::trunc);
    }

    // Publish data to the Connector
    virtual void Publish(const Inquiry<V>& data)
    {
        ofstream out(file_name, ios::app);
        // get current epoch time and output relevent data
        milliseconds ms = duration_cast< milliseconds >(system_clock::now().time_since_epoch());

        string state;
        InquiryState state_enum = data.GetState();
        if(state_enum == RECEIVED)
            state = "RECEIVED";
        else if(state_enum == QUOTED)
            state = "QUOTED";
        else if(state_enum == DONE)
            state = "DONE";

        string side;
        auto side_enum = data.GetSide();
        if(side_enum == BUY)
            side = "BUY";
        else
            side = "SELL";

        out << ms.count() << "," << "TID_" << data.GetInquiryId()
            << "," << data.GetProduct().GetTicker()<< "," << side
            << "," << data.GetPrice() << "," << state << endl;
    }
};


#endif //UNTITLED_CONNECTORS_HPP
