/**
 * positionservice.hpp
 * Defines the data types and Service for positions.
 *
 * @author Breman Thuraisingham
 */
#ifndef POSITION_SERVICE_HPP
#define POSITION_SERVICE_HPP

#include <string>
#include <map>
#include "soa.hpp"
#include "tradebookingservice.hpp"
#include "ProductMap.hpp"

using namespace std;

/**
 * Position class in a particular book.
 * Type T is the product type.
 */
template<typename T>
class Position
{

public:

  // ctor for a position, in the constructor, for each trader/book, set the initial position to 0
  explicit Position(const T &_product);

  // Get the product
  const T& GetProduct() const;

  // Get the position quantity
  long GetPosition(string &book) const;

  // Get the aggregate position
  long GetAggregatePosition() const;

    void UpdatePosition(string& book, long quantity, Side side)
    {
        if(side == BUY)
        {
            positions.find(book)->second += quantity;
        }
        else if(side == SELL)
        {
            positions.find(book)->second -= quantity;
        }
    }

public:
  T product;
  map<string,long> positions;

};

/**
 * Position Service to manage positions across multiple books and secruties.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class PositionService : public Service<string,Position <T>>
{
private:
    unordered_map<string, Position<T>> name_pos;
public:
    // constructor, initialize the position of each product to be 0
    // create the name_pos map: get all of the products in exchange and then initalize the position for each product
    PositionService()
    {
        // get all product types
        unordered_map<string, Bond> bond_map = ProductMap::GetProductMap();
        vector<string> tickers = ProductMap::GetTickers();

        // initialize the position for each product
        for(string ticker: tickers)
        {
            Position<Bond> temp(bond_map.find(ticker)->second);
            auto pair = make_pair(ticker, Position<Bond>(bond_map.find(ticker)->second));
            name_pos.insert(pair);
        }
    }

    // Add a trade to the service
    virtual void AddTrade(const Trade<T> &trade)
    {
        // get a string from the trade
        string ticker = trade.GetProduct().GetTicker();
        //cout << "position service, ticker is " << ticker << endl;

        string book = trade.GetBook();
        long quantity = trade.GetQuantity();
        Side side = trade.GetSide();
        if(name_pos.find(ticker) != name_pos.end())
        {
            // update position
            Position<Bond>& pos = name_pos.find(ticker)->second;
            pos.UpdatePosition(book, quantity, side);

        } else{
            cout << "in position service, AddTrade(), key = " << ticker << "doesn't exist!" << endl;
        }

        // notify the listeners new data is arriving
        Service<string,Position <T>>::Notify(name_pos.find(ticker)->second);
    }

    // Get data on our service given a key
    virtual Position <T>& GetData(string key)
    {
        if(name_pos.find(key) != name_pos.end())
            return name_pos.find(key)->second;
        else
            cout << "in position service, key = " << key << "is not found" << endl;
    }

};


template<typename T>
Position<T>::Position(const T &_product) :
  product(_product)
{
    string books[3]{"TRSY1", "TRSY2", "TRSY3"};
    for(int i = 0; i < 3; i++)
    {
        positions.insert(make_pair(books[i], 0));
    }
}

template<typename T>
const T& Position<T>::GetProduct() const
{
    return product;
}

template<typename T>
long Position<T>::GetPosition(string &book) const
{
    if(positions.find(book) == positions.end())
        cout << "not found!!" << book << endl;
    return positions.find(book)->second;

}

template<typename T>
long Position<T>::GetAggregatePosition() const
{
    string books[3]{"TRSY1", "TRSY2", "TRSY3"};
    long aggregate = 0;
    for(int i = 0; i < 3; i++)
        aggregate += positions.find(books[i])->second;
    return aggregate;
}


#endif
