
/*
 * This is the final project of MTH9814 tutored by Breman Thuraisingham
 * Bond Trading System
 *
 * The bond trading system contains three data flows, which are TradesFlow(), PricesFlow()
 * and InquiryFlow(). They are using different source data(i.e. trades.txt, prices.txt,
 * marketdata.txt)
 *
 * Since they are independent in terms of the data flow, they are seperated into three
 * functions
 *
 * In main function, we can either choose some of the data flow to run, or run all of them
 *
 * The time stamps used in recoding historical data are the difference,
 * measured in milliseconds, between the current time and midnight, January 1, 1970 UTC.
 *
 *
 *
 * The ouput files are under directory BondTradingSystem/output
 * Input data files trades.txt, prices.txt, marketdata.txt can be found under directory
 * /data_generators. If we want to regenerate the data, we can run the python script
 * trades_generator.py which is also under directory BondTradingSystem/data_generators
 *
 * Thank you for your patience to run the project and wait for the result
 */
#include<iostream>
#include "products.hpp"
#include "positionservice.hpp"
#include "tradebookingservice.hpp"
#include "soa.hpp"
#include "ListenersImpl.hpp"
#include "pricingservice.hpp"
#include "riskservice.hpp"
#include "ConnectorsImpl.hpp"
#include "GUIService.hpp"
#include "historicaldataservice.hpp"
#include "BondAlgoExecutionService.hpp"
#include "executionservice.hpp"
#include "inquiryservice.hpp"

using namespace std;

/*
 * This part is for the data flow started with trades.txt
 * Two branches:
 * 1. trades.txt-> trade booking service -> position service -> risk service
 * -> historical data service -> risk.txt
 *
 * 2. trades.txt-> trade booking service -> position service -> historical data service
 * -> position.txt
 */

void TradesFlow()
{
    cout << "inside trade booking flow test" << endl;
    TradeBookingService<Bond> trade_booking_service;

    // Connect position service listener to trade booking service
    PositionService<Bond> pos_service;
    PositionServiceListener<Bond> position_listener(&pos_service);
    trade_booking_service.AddListener(&position_listener);


    PositionConnector<Bond> position_connector("../output/history_position_first60.txt");
    PositionHistoricalData<Bond> position_history(&position_connector);
    HistPositionListener<Bond> history_position_listener(&position_history);

    // enable data flow from position service to history position service
    pos_service.AddListener(&history_position_listener);


    // add the risk listener to position service
    RiskService<Bond> risk_service;
    RiskServiceListener<Bond> risk_service_listener(&risk_service);
    pos_service.AddListener(&risk_service_listener);


    // create risk historical data connector to publish data
    // add the historical risk listener to risk service
    RiskConnector<Bond> risk_connector("../output/risk_first60.txt");
    RiskHistoricalData<Bond> risk_history(&risk_connector);
    HistRiskListener<Bond> history_risk_listener(&risk_history);
    risk_service.AddListener(&history_risk_listener);


    // ***************************** Let's run it! ******************************
    // start to run the 60 trades!
    // input the trades.txt
    TradeBookingConnector<Bond> trade_connector("../data_generators/trades.txt", &trade_booking_service);

//    TradeBookingConnector<Bond> trade_connector("../naive_trades.txt", &trade_booking_service);
    // traverse the 60 trades in trades.txt
    trade_connector.TraverseTrades();
}


/*
 * This part is for the data flow started with prices.txt
 * Two branches:
 * 1. prices.txt-> bond pricing service -> gui service -> output gui.txt
 * 2. prices.txt-> bond pricing service -> algo streaming service -> streaming service
 *    -> historical data service -> streaming.txt
 */
void PricesFlow()
{
    // attach the gui connector to gui service
    GUIConnector<Bond> gui_connector("../output/gui.txt");
    GUIService<Bond> gui_service(&gui_connector);

    // attach the gui service to gui listener
    GUIListener<Bond> gui_listener(&gui_service);

    // initialize the pricing service and add the gui listener for communication between pricing service and gui service
    PricingService<Bond> pricing_service;
    pricing_service.AddListener(&gui_listener);


    // Using listener to build connection between pricing service and algo streaming service
    BondAlgoStreamingService<Bond> bond_algo_streaming_service;
    AlgoStreamingListener<Bond> algo_streaming_listener(&bond_algo_streaming_service);
    pricing_service.AddListener(&algo_streaming_listener);

    // Using listener to build connection between algo streaming service and streaming service
    StreamingService<Bond> streaming_service;
    StreamingListener<Bond> streaming_listener(&streaming_service);
    bond_algo_streaming_service.AddListener(&streaming_listener);

    // historical data and bond streaming service
    StreamingConnector<Bond> streaming_connector("../output/streaming.txt");
    StreamingHistoricalDataService<Bond> streaming_hist_service(&streaming_connector);
    HistStreamingListener<Bond> hist_streaming_listener(&streaming_hist_service);

    streaming_service.AddListener(&hist_streaming_listener);


    // *************************** let's run it ***************************
    // initialize the connector of the input file
    // iterate to get all prices.txt info and flow to pricing service
    PricingConnector<Bond> pricing_connector("../data_generators/prices.txt", &pricing_service);
    pricing_connector.Subscribe();


}

/*
 * This part is for the data flow started with marketdata.txt
 * Data flow is as following
 *     1. marketdata.txt-> market data service -> Bond algo execution service
 *        -> bond execution service -> trade booking service
 *     2. after reaching trade booking service, the data flow is exactly the same as
 *        in the TradesFlow() function described
 */
void MarketDataFlow()
{

    BondAlgoExecutionService<Bond> bond_algo_execution_service;
    BondAlgoExecutionListener<Bond> bond_algo_execution_listener(&bond_algo_execution_service);

    MarketDataService<Bond> mrkt_data_service;
    mrkt_data_service.AddListener(&bond_algo_execution_listener);

    // create the execution service and connect to algo execution via a listener
    ExecutionService<Bond> execution_service;
    ExecutionServiceListener<Bond> execution_service_listener(&execution_service);
    bond_algo_execution_service.AddListener(&execution_service_listener);

    // build the connection between trade booking service and execution service via listener
    TradeBookingService<Bond> trade_booking_service;
    TradeBookingServiceListener<Bond> trade_booking_listener(&trade_booking_service);
    execution_service.AddListener(&trade_booking_listener);


    //**************************** down is the previous flow same as trade flow ******
    // Connect position service listener to trade booking service
    PositionService<Bond> pos_service;
    PositionServiceListener<Bond> position_listener(&pos_service);
    trade_booking_service.AddListener(&position_listener);


    PositionConnector<Bond> position_connector("../output/positions.txt");
    PositionHistoricalData<Bond> position_history(&position_connector);
    HistPositionListener<Bond> history_position_listener(&position_history);

    // enable data flow from position service to history position service
    pos_service.AddListener(&history_position_listener);


    // add the risk listener to position service
    RiskService<Bond> risk_service;
    RiskServiceListener<Bond> risk_service_listener(&risk_service);
    pos_service.AddListener(&risk_service_listener);


    // create risk historical data connector to publish data
    // add the historical risk listener to risk service
    RiskConnector<Bond> risk_connector("../output/risk.txt");
    RiskHistoricalData<Bond> risk_history(&risk_connector);
    HistRiskListener<Bond> history_risk_listener(&risk_history);
    risk_service.AddListener(&history_risk_listener);
    //**************************** up is the previous flow same as trade flow ******


    // collect the historical data from execution service
    ExecutionConnector<Bond> execution_connector("../output/executions.txt");
    ExecutionHistoricalService<Bond> execution_hist_service(&execution_connector);
    ExecutionHistoricalDataServiceListener<Bond> execution_hist_service_listener(&execution_hist_service);
    execution_service.AddListener(&execution_hist_service_listener);


    // ******************** input the data from marketdata.txt and run

    MrktDataConnector<Bond> mrkt_data_connector("../data_generators/marketdata.txt", &mrkt_data_service);
    mrkt_data_connector.Subscribe();



}


/* handling the inquiry flow. It starts from reading data from inquiries.txt
 * Then pass the data to BondInquiryService and change the state until it
 * reached the state of DONE
 */
void InquiryFlow()
{

    AllInquiriesConnector<Bond> all_inquiries_connector("../output/allinquiries.txt");
    InquiryHistoricalService<Bond> inquiry_hist_service(&all_inquiries_connector);

    AllInquiryHistoricalDataServiceListener<Bond> all_inquiry_hist_listner(&inquiry_hist_service);


    InquiryService<Bond> inquiry_service;
    inquiry_service.AddListener(&all_inquiry_hist_listner);

    InquiryConnector<Bond> inquiry_connector("../data_generators/inquiries.txt", &inquiry_service);


    inquiry_connector.Subscribe();
}

int main()
{

    // handling the first 60 trades flowed in from trades.txt
    TradesFlow();

    // prices flow, starting with prices.txt
    // output gui.txt and streaming.txt
    PricesFlow();

    // market data flow starts with marketdata.txt
    MarketDataFlow();

    // inquiry flow starts with inquires.txt
    InquiryFlow();


    cout << "Bond Trading System has been shut down properly" << endl;
    cout << "All tasks completed" << endl;
    cout << "Thank you for your patience" << endl;
}