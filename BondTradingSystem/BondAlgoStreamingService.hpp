//
// Created by ziyi on 12/15/17.
//

//BondAlgoStreamingService
//        This should be keyed on product identifier with value an AlgoStream object.
// The AlgoStream should be a class with a reference to a PriceStream object.


#ifndef BONDTRADINGSYSTEM_BONDALGOSTREAMINGSERVICE_HPP
#define BONDTRADINGSYSTEM_BONDALGOSTREAMINGSERVICE_HPP

#include "soa.hpp"
#include "streamingservice.hpp"
#include "pricingservice.hpp"
#include "products.hpp"

/*
 * V is the product type
 */
template<typename V>
class BondAlgoStreamingService: public Service<string,PriceStream <V> >
{
public:
    void PublishPrice(Price<V>& data)
    {
        // use the price data to get the bid/ask price and then create the order

        double bid_price = data.GetMid() - data.GetBidOfferSpread()/2;
        double ask_price = data.GetMid() + data.GetBidOfferSpread()/2;

        PriceStreamOrder bid_order(bid_price, 1000000, 1000000, BID);
        PriceStreamOrder ask_order(ask_price, 1000000, 1000000, OFFER);
        PriceStream<Bond> price_stream(data.GetProduct(), bid_order, ask_order);

        Service<string,PriceStream <V> >::Notify(price_stream);
    }

};
#endif //BONDTRADINGSYSTEM_BONDALGOSTREAMINGSERVICE_HPP
