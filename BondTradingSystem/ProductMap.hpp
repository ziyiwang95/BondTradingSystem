/*
 * This file contains the ProductMap class, it is used to provide
 * product information and ticker information
 * If in the future the ticker for certain product changes, modifications only need to be
 * done here due to the encapsolation property in this project
 */

#ifndef PRODUCTMAP_HPP
#define PRODUCTMAP_HPP

#include <vector>
#include <unordered_map>
#include "products.hpp"

/*
 * give a product ID, return the corresponding product
 * return a map containing all of the products traded
 */
class ProductMap
{
public:
    static unordered_map<string, Bond> GetProductMap()
    {
        unordered_map<string, Bond> product_map;  // give a ticker, return a product
        vector<Bond> bonds;

        bonds.push_back( Bond("9128283H1", CUSIP, "9128283H1", 0.05, date(2019,11,30)));
        bonds.push_back( Bond("9128283G3", CUSIP, "9128283G3", 0.05, date(2020,11,30)));
        bonds.push_back( Bond("912828M80", CUSIP, "912828M80", 0.05, date(2022,11,30)));
        bonds.push_back( Bond("9128283J7", CUSIP, "9128283J7", 0.05, date(2024,11,30)));
        bonds.push_back( Bond("9128283F5", CUSIP, "9128283F5", 0.05, date(2027,11,30)));
        bonds.push_back( Bond("912810RZ3", CUSIP, "912810RZ3", 0.05, date(2047,11,30)));

        for(auto bond: bonds)
        {
            auto pair = make_pair(bond.GetProductId(), bond);
            product_map.insert(pair);
        }
        return product_map;
    }

    static vector<Bond> GetProducts()
    {
        vector<Bond> bonds;
        bonds.push_back( Bond("2y", CUSIP, "2y", 0.05, date(2019,11,30)));
        bonds.push_back( Bond("3y", CUSIP, "3y", 0.05, date(2020,11,30)));
        bonds.push_back( Bond("5y", CUSIP, "5y", 0.05, date(2022,11,30)));
        bonds.push_back( Bond("7y", CUSIP, "7y", 0.05, date(2024,11,30)));
        bonds.push_back( Bond("10y", CUSIP, "10y", 0.05, date(2027,11,30)));
        bonds.push_back( Bond("30y", CUSIP, "30y", 0.05, date(2047,11,30)));
        return bonds;
    };

    static vector<string> GetTickers()
    {
        return vector<string>{"9128283H1", "9128283G3", "912828M80", "9128283J7", "9128283F5", "912810RZ3"};
    }
};
#endif //PRODUCTMAP_HPP
