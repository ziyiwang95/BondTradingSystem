# README #

### What is this repository for? ###

The repo is for the final project: bond trading system

### Who do I talk to? ###

Repo owner: Ziyi Wang
Email address: ziyiwang95@gmail.com


### How to generate data as input? ###

In the folder the data has been generated already, if you would like to regenearte data, please use the following commands and the data will be in
the directory /data_generators

cd data_generators
python trades_generator.py

### How to run? ###

The project has been built using CLion
To run the code, please using the following commands:

cd BondTradingSystem/cmake-build-debug
./BondTradingSystem

### How to see the results? ###

The results are stored in the folder /output.
It would normally run around ten minutes. Thank you for your patience.