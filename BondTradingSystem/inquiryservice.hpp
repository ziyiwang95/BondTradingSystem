/**
 * inquiryservice.hpp
 * Defines the data types and Service for customer inquiries.
 *
 * @author Breman Thuraisingham
 */
#ifndef INQUIRY_SERVICE_HPP
#define INQUIRY_SERVICE_HPP

#include "soa.hpp"
#include "tradebookingservice.hpp"


// Various inqyury states
enum InquiryState { RECEIVED, QUOTED, DONE, REJECTED, CUSTOMER_REJECTED };

/**
 * Inquiry object modeling a customer inquiry from a client.
 * Type T is the product type.
 */
template<typename T>
class Inquiry
{

public:

  // ctor for an inquiry
  Inquiry(string _inquiryId, const T &_product, Side _side, long _quantity, double _price, InquiryState _state);

  // Get the inquiry ID
  const string& GetInquiryId() const;

  // Get the product
  const T& GetProduct() const;

  // Get the side on the inquiry
  Side GetSide() const;

  // Get the quantity that the client is inquiring for
  long GetQuantity() const;

  // Get the price that we have responded back with
  double GetPrice() const;

  // Get the current state on the inquiry
  InquiryState GetState() const;

    void SetPrice(double price_)
    {
        price = price_;
    }

    void SetState(InquiryState new_state)
    {
        state = new_state;
    }

private:
  string inquiryId;
  T product;
  Side side;
  long quantity;
  double price;
  InquiryState state;

};

/**
 * Service for customer inquirry objects.
 * Keyed on inquiry identifier (NOTE: this is NOT a product identifier since each inquiry must be unique).
 * Type T is the product type.
 */

template<typename T>
class InquiryService : public Service<string,Inquiry <T> >
{
private:

public:
    InquiryService()
    {}

    // Send a quote back to the client
    void SendQuote(const string &inquiryId, double price)
    {
      //TBD
    }

    // Reject an inquiry from the client
    void RejectInquiry(const string &inquiryId)
    {
      //TBD
    }

    virtual void OnMessage(Inquiry<T> &data)
    {
        // after getting the data, the inquiry gives a quote and send back the inquiry
        InquiryState state = data.GetState();

        // if the state is RECEIVED, quote a price and send back to the connector
        if(state == RECEIVED)
        {
            string inquiry_id = data.GetInquiryId();
            data.SetPrice(100.0);
            Service<string,Inquiry <T>>::Notify(data);
            //connector->Publish(data);
        }
        // if this inquiry has already been quoted, just pass the inquiry to listeners
        else if(state == QUOTED || state == DONE)
        {
            Service<string,Inquiry <T>>::Notify(data);
        }
    }
};

template<typename T>
Inquiry<T>::Inquiry(string _inquiryId, const T &_product, Side _side, long _quantity, double _price, InquiryState _state) :
  product(_product)
{
  inquiryId = _inquiryId;
  side = _side;
  quantity = _quantity;
  price = _price;
  state = _state;
}

template<typename T>
const string& Inquiry<T>::GetInquiryId() const
{
  return inquiryId;
}

template<typename T>
const T& Inquiry<T>::GetProduct() const
{
  return product;
}

template<typename T>
Side Inquiry<T>::GetSide() const
{
  return side;
}

template<typename T>
long Inquiry<T>::GetQuantity() const
{
  return quantity;
}

template<typename T>
double Inquiry<T>::GetPrice() const
{
  return price;
}

template<typename T>
InquiryState Inquiry<T>::GetState() const
{
  return state;
}

#endif
