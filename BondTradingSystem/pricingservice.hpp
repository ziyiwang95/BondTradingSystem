/**
 * pricingservice.hpp
 * Defines the data types and Service for internal prices.
 *
 * @author Breman Thuraisingham
 */
#ifndef PRICING_SERVICE_HPP
#define PRICING_SERVICE_HPP

#include <string>
#include "soa.hpp"
/**
 * A price object consisting of mid and bid/offer spread.
 * Type T is the product type.
 */
template<typename T>
class Price
{

public:

  // ctor for a price
  Price(const T &_product, double _mid, double _bidOfferSpread);

  // Get the product
  const T& GetProduct() const;

  // Get the mid price
  double GetMid() const;

  // Get the bid/offer spread around the mid
  double GetBidOfferSpread() const;

private:
  const T& product;
  double mid;
  double bidOfferSpread;

};

/**
 * Pricing Service managing mid prices and bid/offers.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class PricingService : public Service<string,Price <T> >
{
private:
    map<string, Price<T>> prices;
public:
    // Get data on our service given a key
    virtual Price <T>& GetData(string key)
    {
        if(prices.find(key) != prices.end())
            return prices.find(key)->second;
        else
            cout << "in pricing service, key = " << key << "not found!" << endl;
    }

    // The callback that a Connector should invoke for any new or updated data
    virtual void OnMessage(Price <T> &data)
    {
        // update price map
        prices.erase(data.GetProduct().GetTicker());
        prices.insert(make_pair(data.GetProduct().GetTicker(), data));

        // notify listeners to get the data
        Service<string,Price <T> >:: Notify(data);
    }
};

template<typename T>
Price<T>::Price(const T &_product, double _mid, double _bidOfferSpread) :
  product(_product)
{
  mid = _mid;
  bidOfferSpread = _bidOfferSpread;
}

template<typename T>
const T& Price<T>::GetProduct() const
{
  return product;
}

template<typename T>
double Price<T>::GetMid() const
{
  return mid;
}

template<typename T>
double Price<T>::GetBidOfferSpread() const
{
  return bidOfferSpread;
}

#endif
