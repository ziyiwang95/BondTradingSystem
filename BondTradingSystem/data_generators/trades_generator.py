

import random
#Example of fractional is 100-xyz, with xy being from 0 to 31 and z being from 0 to 7
# The xy number gives the decimal out of 32 and the z number gives the remainder out of 256.
# So 100-001 is 100.00390625 in decimal and 100-25+ is 100.796875 in decimal.


tickers = ["9128283H1", "9128283G3", "912828M80", "9128283J7", "9128283F5", "912810RZ3"]

# Create 10 trades for each security (so a total of 60 trades across all 6 securities)
# in the file with the relevant trade attributes.
# Positions should be across books TRSY1, TRSY2, and TRSY3.

# output trades.txt
# lines looks like: 2y, TID_3, TRSY2, 5000000, 99.0, BUY
def trade_generator(path):
    print("generating trades.txt")
    f = open(path, "w")

    books = [" TRSY1", " TRSY2", " TRSY3"]

    side = [" BUY", " SELL"]

    trade_id = 0

    for ticker in tickers:
        for i in range(10):
            elements = []  # contains the elements of the trade
            trade_id += 1

            elements.append(ticker)

            trade_id_str = " TID" + (str)(trade_id)
            elements.append(trade_id_str)

            # pick the book in turn
            elements.append(books[i % 3])

            # set the fixed quantity to be a million
            elements.append(" " + (str)(1000000))

            # set the price
            elements.append(" " + (str)(99.0))

            # set the side of trade
            elements.append(side[i % 2])

            f.write(",".join(elements) + "\n")

    f.close()
    print("finished")

"""
get random price between 99 and 101
e.g. 100-001
The bid/offer spread should oscillate between 1/128 and 1/64.
"""
def get_random_price():
    get_random_price.counter += 1
    integer_part = random.randint(99,100)
    decimal_32 = random.randint(10,31)       # 1/32
    decimal_8 = random.randint(0,3)          # 1/256

    bid_str = (str)(integer_part) + "-" + (str)(decimal_32) + (str)(decimal_8)
    if get_random_price.counter%2 == 0:
        # spread = 1/128
        ask_str = (str)(integer_part) + "-" + (str)(decimal_32) + (str)(decimal_8+2)
    else:
        # spread = 1/64
        ask_str = (str)(integer_part) + "-" + (str)(decimal_32) + (str)(decimal_8 + 4)

    return (bid_str, ask_str)

get_random_price.counter = 0
"""
Generate prices.txt, contains 1,000,000 prices for each security, total 6000000
prices osillate between 99 and 101
each line:
ticker1 bid ask ticker2 bid ask...
"""
def price_generator(path):
    print("generating prices.txt")
    f = open(path, "w")
    price_num = 1000000
    for i in range(1000000):
        for ticker in tickers:
            line_elements = []
            line_elements.append(ticker)

            (bid_str, ask_str) = get_random_price()
            line_elements.append(bid_str)
            line_elements.append(ask_str)

            f.write(",".join(line_elements)+"\n")
    f.close()

    print("finished")


"""
5 orders deep on both bid and offer stacks.
top to bottom 10 mil, 20 mil, ..., 50 mil
mid prices which oscillate between 99 and 101
1/128th until it reaches 1/32nd, and then decrease back down to 1/128th in 1/128th intervals 

"""
def market_data_generator(path):
    print("generating marketdata.txt")
    f = open(path, "w")

    order_book_each_bond = 1000000

    cnt = -1
    spread = [2,4,6,8,6,4]        # spread from 1/128, to 4/128 and back to 1/128, 2 means 1/256*2
    for i in range(1,order_book_each_bond+1):
        if i % 100000 == 0:
            print((str)(i/10000) + "% order books finished generating")
        for ticker in tickers:

            output_line = ticker
            # get mid price
            integer_part = random.randint(99, 100)
            decimal_32 = random.randint(10, 30)  # 1/32
            decimal_8 = random.randint(4, 7)  # 1/256

            #print(integer_part,decimal_32,decimal_8)
            cnt += 1
            top_spread = spread[cnt%6]

            up_spread = top_spread/2
            down_spread = top_spread/2

            #print(up_spread, down_spread)
            for k in range(5):
                # in the level k order book,
                bid_decimal_8 = decimal_8 - down_spread
                bid_decimal_32 = decimal_32
                if decimal_8 + up_spread >= 8:
                    ask_decimal_8 = decimal_8 + up_spread - 8
                    ask_decimal_32 = decimal_32 + 1
                else:
                    ask_decimal_8 = decimal_8
                    ask_decimal_32 = decimal_32

                bid = (str)(integer_part) + "-" + (str)(bid_decimal_32) + (str)(bid_decimal_8)
                ask = (str)(integer_part) + "-" + (str)(ask_decimal_32) + (str)(ask_decimal_8)
                output_line += "," + bid + "," + ask
                # next level, spread increase
                up_spread += 1
            # output an order book
            f.write(output_line+ "\n")
    print("finished")
    f.close()


def inquiries_generator(path):
    directions = ["BUY", "SELL"]

    f = open(path, "w")
    counter = 0
    for i in range(10):
        for ticker in tickers:
            counter += 1
            direction = directions[counter%2]
            f.write((str)(counter) + "," + ticker + "," + direction + ",RECEIVED\n")

    f.close()


if __name__ == "__main__":
    #trade_generator("trades.txt")

    #price_generator("prices.txt")

    #market_data_generator("marketdata.txt")

    inquiries_generator("inquiries.txt")