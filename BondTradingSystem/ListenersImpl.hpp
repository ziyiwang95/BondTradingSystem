/*
 *
 * This file defines all of the listeners
 */

#ifndef DERIVED_SERVICE
#define DERIVED_SERVICE

#include "positionservice.hpp"
#include "riskservice.hpp"
#include "historicaldataservice.hpp"
#include "GUIService.hpp"
#include "BondAlgoStreamingService.hpp"
#include "BondAlgoExecutionService.hpp"
#include "executionservice.hpp"

/**
* Bond trade booking listener
T is product type
*/
template<typename T>
class PositionServiceListener : public ServiceListener<Trade<T>>
{
private:
	PositionService<T>* service;
public:

    explicit PositionServiceListener(PositionService<T>* service_) : service(service_)
	{}

	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(Trade<T> &data)
	{
		service->AddTrade(data);
	}

	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(Trade<T> &data)
	{}

	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(Trade<T> &data)
	{}
};

template<typename T>
class RiskServiceListener : public ServiceListener<Position<T>>
{
private:
    RiskService<T>* service;
public:

    explicit RiskServiceListener(RiskService<T>* service_): service(service_)
    {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(Position<T> &data)
    {
        service->AddPosition(data);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(Position<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(Position<T> &data)
    {}
};

template<typename T>
class HistPositionListener: public ServiceListener<Position<T>>
{
private:
    PositionHistoricalData<T>* service;
public:

    explicit  HistPositionListener(PositionHistoricalData<T>* service_): service(service_)
            {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(Position<T> &data)
    {
        service->PersistData("key", data);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(Position<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(Position<T> &data)
    {}
};

template<typename T>
class HistRiskListener: public ServiceListener<PV01<T>>
{
private:
    RiskHistoricalData<T>* service;
public:
    explicit HistRiskListener(RiskHistoricalData<T>* service_): service(service_)
    {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(PV01<T> &data)
    {
        service->PersistData("key", data);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(PV01<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(PV01<T> &data)
    {}
};


template<typename T>
class HistStreamingListener:public ServiceListener<PriceStream<T>>
{
private:
    StreamingHistoricalDataService<T>* service;
public:
    explicit HistStreamingListener(StreamingHistoricalDataService<T>* service_): service(service_)
            {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(PriceStream<T> &data)
    {
        service->PersistData("key", data);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(PriceStream<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(PriceStream<T> &data)
    {}

};



template<typename T>
class GUIListener: public ServiceListener<Price<T>>
{
private:
    GUIService<T>* service;
public:
    explicit GUIListener(GUIService<T>* service_): service(service_)
    {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(Price<T> &data)
    {
        service->ProvideData(data);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(Price<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(Price<T> &data)
    {}
};

template<typename T>
class AlgoStreamingListener:public ServiceListener<Price<T>>
{
private:
    BondAlgoStreamingService<T>* service;
public:
    explicit AlgoStreamingListener(BondAlgoStreamingService<T>* service_): service(service_)
            {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(Price<T> &data)
    {
        service->PublishPrice(data);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(Price<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(Price<T> &data)
    {}
};

template<typename T>
class StreamingListener:public ServiceListener<PriceStream <T>>
{
private:
    StreamingService<T>* service;
public:
    explicit StreamingListener(StreamingService<T>* service_): service(service_)
    {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(PriceStream <T> &data)
    {
        service->PublishPrice(data);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(PriceStream <T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(PriceStream <T> &data)
    {}
};


template<typename T>
class BondAlgoExecutionListener:public ServiceListener<OrderBook <T>>
{
private:
    BondAlgoExecutionService<T>* service;
public:
    explicit BondAlgoExecutionListener(BondAlgoExecutionService<T>* service_): service(service_)
            {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(OrderBook<T> &data)
    {
        service->Execute(data);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(OrderBook<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(OrderBook<T> &data)
    {}
};

template<typename T>
class ExecutionServiceListener:public ServiceListener<AlgoExecution<T>>
{
private:
    ExecutionService<T>* service;
public:
    explicit ExecutionServiceListener(ExecutionService<T>* service_): service(service_)
    {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(AlgoExecution<T> &algo_execution)
    {
        service->ExecuteOrder(algo_execution.GetOrder(), algo_execution.GetMarket());
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(AlgoExecution<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(AlgoExecution<T> &data)
    {}
};


template<typename T>
class TradeBookingServiceListener:public ServiceListener<ExecutionOrder <T>>
{
private:
    TradeBookingService<T>* service;
public:
    explicit TradeBookingServiceListener(TradeBookingService<T>* service_): service(service_)
    {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(ExecutionOrder <T> &algo_execution)
    {
        // convert Execution Order to the trade and send it to book trading service

        // collecting the information to formulate the trade object
        T product = algo_execution.GetProduct();
        string trade_id = algo_execution.GetOrderId();
        double price = algo_execution.GetPrice();

        int id = std::stoi(trade_id);
        vector<string> books{"TRSY1", "TRSY2", "TRSY3"};
        string book = books[id%3];

        long quantity = algo_execution.GetVisibleQuantity();

        PricingSide side = algo_execution.GetPricingSide();
        Side order_side;
        if(side == BID)
            order_side = BUY;
        else
            order_side = SELL;

        Trade<T> trade(product, trade_id, price, book, quantity, order_side);
        service->BookTrade(trade);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(ExecutionOrder <T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(ExecutionOrder <T> &data)
    {}
};


template<typename T>
class ExecutionHistoricalDataServiceListener:public ServiceListener<ExecutionOrder<T>>
{
private:
    ExecutionHistoricalService<T>* service;
public:
    explicit ExecutionHistoricalDataServiceListener(ExecutionHistoricalService<T>* service_): service(service_)
    {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(ExecutionOrder<T> &algo_execution)
    {
        service->PersistData("key", algo_execution);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(ExecutionOrder<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(ExecutionOrder<T> &data)
    {}
};

template<typename T>
class AllInquiryHistoricalDataServiceListener:public ServiceListener<Inquiry<T>>
{
private:
    InquiryHistoricalService<T>* service;
public:
    explicit AllInquiryHistoricalDataServiceListener(InquiryHistoricalService<T>* service_): service(service_)
    {}

    // Listener callback to process an add event to the Service
    virtual void ProcessAdd(Inquiry<T> &data)
    {
        service->PersistData("key", data);
    }

    // Listener callback to process a remove event to the Service
    virtual void ProcessRemove(Inquiry<T> &data)
    {}

    // Listener callback to process an update event to the Service
    virtual void ProcessUpdate(Inquiry<T> &data)
    {}
};



#endif // !DERIVED_SERVICE