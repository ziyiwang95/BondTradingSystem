/*
 * The FormatParser class handles the transform of different types of data
 * Transform from raw input lines from files to vectors contain elements
 * Transform from string type prices to numerical prices and vice versa
 */

#ifndef FILEPARSER_HPP
#define FILEPARSER_HPP

#include<vector>
#include<string>
#include <tuple>

using std::vector;
using std::string;
using std::tuple;

/*
 * parse the raw string read from file to specific format
*/
class FormatParser
{
public:

    // parse the text
    static vector<string> Parse(const string& text)
    {
        vector<string> res;
        int i = 0;
        istringstream iss(text);
        string token;

        while (getline(iss, token,','))
        {
            iss.ignore();    // ignore the space following the comma
            res.push_back(token);
//            cout << "parsed " <<  res[i] << endl;
            i++;
        }
        return res;
    }

    // parse the text, only seperated by comma, no spaces in between
    static vector<string> ParseCommaSepLine(const string& text)
    {
        vector<string> res;
        int i = 0;
        istringstream iss(text);
        string token;

        while (getline(iss, token,','))
        {
            res.push_back(token);
            i++;
        }
        return res;
    }

    // input a string type price and return the numerical value of the price
    static double ParsePriceFormat(const string& price_string)
    {
        const char* price_char = price_string.c_str();

        int price_parts[3];
        int part2_start_pos;
        if(price_char[0] == '9')
        {
            price_parts[0] = 99;
            part2_start_pos = 3;
        }
        else
        {
            price_parts[0] = 100;
            part2_start_pos = 4;
        }
        price_parts[1] = (price_char[part2_start_pos]-'0') * 10 + (price_char[part2_start_pos+1] - '0');
        price_parts[2] = (price_char[part2_start_pos+2]-'0');

        return price_parts[0] + price_parts[1] / 32.0 + price_parts[2] / 256.0;
    }

    // parse the raw input from marketdata.txt, which are order books and return the price

    static tuple<string, vector<double>> ParseOrderBook(const string& text)
    {
        vector<string> string_res = ParseCommaSepLine(text);
        vector<double> prices;
        string ticker = string_res[0];

        for(int i = 1; i <= 10; i++)
        {
            prices.push_back(ParsePriceFormat(string_res[i]));
        }
        return std::make_tuple(ticker, prices);
    };
};
#endif //FILEPARSER_HPP